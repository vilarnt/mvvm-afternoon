//
//  MVVMLearningAfternoonApp.swift
//  MVVMLearningAfternoon
//
//  Created by Vilar da Camara Neto on 06/12/22.
//

import SwiftUI

@main
struct MVVMLearningAfternoonApp: App {
    var body: some Scene {
        WindowGroup {
            let service = JSONMovieService()
            let viewModel = MovieListViewModel(service: service)
            MovieListView(viewModel: viewModel)
        }
    }
}
