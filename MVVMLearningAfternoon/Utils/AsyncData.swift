//
//  AsyncData.swift
//  MVVMLearningAfternoon
//
//  Created by Vilar Fiuza da Camara Neto on 13/12/22.
//

import Foundation


enum AsyncData<Dados> {
    case loading
    case loaded(Dados)
    case failed
}
