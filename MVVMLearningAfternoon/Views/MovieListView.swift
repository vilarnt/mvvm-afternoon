//
//  ContentView.swift
//  MVVMLearningAfternoon
//
//  Created by Vilar da Camara Neto on 06/12/22.
//

import SwiftUI

struct MovieListView: View {
    @ObservedObject private var viewModel: MovieListViewModel

    init(viewModel: MovieListViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        switch self.viewModel.allMovies {
        case .loading:
            Text("Loading…")
        case .failed:
            Text("Could not retrieve server data.")
        case .loaded(let allMovies):
            List(allMovies) { movie in
                VStack(alignment: .leading, spacing: 8) {
                    Text(movie.origTitle)
                        .bold()
                    if let localizedTitle = movie.localizedTitle {
                        Text(localizedTitle)
                    }
                    Text("\(movie.year)")
                        .foregroundColor(.gray)
                    if let duration = movie.duration {
                        Text("\(duration) minutes")
                            .foregroundColor(.gray)
                    }
                }
            }
            .padding()
        }
    }
}

class MovieListViewModel: ObservableObject {
    @Published var allMovies: AsyncData<[Movie]> = .loading

    private var service: MovieServiceProtocol

    init(service: MovieServiceProtocol) {
        self.service = service
        self.service.getMovies { movies in
            DispatchQueue.main.async {
                if movies != nil {
                    self.allMovies = .loaded(movies!)
                } else {
                    self.allMovies = .failed
                }
            }
        }
    }
}

struct MovieListView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            let service = MockupMovieService(behaviour: .normal)
            let viewModel = MovieListViewModel(service: service)
            MovieListView(viewModel: viewModel)
                .previewDisplayName("Normal")

            let loadingService = MockupMovieService(behaviour: .keepLoading)
            let loadingViewModel = MovieListViewModel(service: loadingService)
            MovieListView(viewModel: loadingViewModel)
                .previewDisplayName("Loading")

            let failureService = MockupMovieService(behaviour: .alwaysFails)
            let failureViewModel = MovieListViewModel(service: failureService)
            MovieListView(viewModel: failureViewModel)
                .previewDisplayName("Failure")
        }
    }
}
