//
//  MockupMovieService.swift
//  MVVMLearningAfternoon
//
//  Created by Vilar da Camara Neto on 06/12/22.
//

import Foundation


class MockupMovieService: MovieServiceProtocol {
    enum Behaviour {
        case normal
        case alwaysFails
        case keepLoading
    }
    
    private let behaviour: Behaviour
    
    init(behaviour: Behaviour = .normal) {
        self.behaviour = behaviour
    }
    
    private static let mockupMovies: [Movie] = [
        .init(id: "1", origTitle: "Back Without Forth", localizedTitle: "A Volta dos que Não Foram", year: 2022, duration: 123),
        .init(id: "2", origTitle: "CBL in a Nutshell", localizedTitle: nil, year: 2022, duration: 45),
        .init(id: "3", origTitle: "Week", localizedTitle: "Sete Dias e Sete Noites", year: 2015, duration: 100),
        .init(id: "4", origTitle: "Original Title", localizedTitle: "Título Original", year: 2022, duration: nil),
        .init(id: "5", origTitle: "The Sidewalk", localizedTitle: "Uma Trilha que Não Se Apaga Depois da Nevasca de Ontem", year: 2018, duration: nil),
    ]
    
    func getMovies(completionHandler: @escaping ([Movie]?) -> Void) {
        switch behaviour {
        case .normal:
            completionHandler(Self.mockupMovies)
        case .alwaysFails:
            completionHandler(nil)
        case .keepLoading:
            break
        }
    }
}
