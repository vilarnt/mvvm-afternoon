//
//  JSONMovieService.swift
//  MVVMLearningAfternoon
//
//  Created by Vilar Fiuza da Camara Neto on 12/12/22.
//

import Foundation


class JSONMovieService: MovieServiceProtocol {
    func getMovies(completionHandler: @escaping ([Movie]?) -> Void) {
        let url = URL(string: "http://localhost:8000/index.json")!
        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Houve um erro: \(error!)")
                completionHandler(nil)
                return
            }
            guard let data = data else {
                print("Dados não disponíveis")
                completionHandler(nil)
                return
            }

            do {
                let movies = try JSONDecoder().decode([Movie].self, from: data)
                completionHandler(movies)
            } catch {
                print("Houve um erro durante a decodificação: \(error)")
                completionHandler(nil)
            }
        }
        
        task.resume()
    }
}
