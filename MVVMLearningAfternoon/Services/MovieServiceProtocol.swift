//
//  MovieServiceProtocol.swift
//  MVVMLearningAfternoon
//
//  Created by Vilar da Camara Neto on 06/12/22.
//

import Foundation


protocol MovieServiceProtocol {
    func getMovies(completionHandler: @escaping ([Movie]?) -> Void)
}
